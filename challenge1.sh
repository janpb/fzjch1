#!/bin/bash
#  -------------------------------------------------------------------------------
#  \author Jan Piotr Buchmann <jpb@members.fsf.org>
#  \copyright 2020
#  -------------------------------------------------------------------------------

set -o errexit -o pipefail -o noclobber -o nounset


# Add challenge to PYTHONPATH for tests
export PYTHONPATH=dag/src/

function run_matrix_test()
{
  printf "Running matrix test\n"
  python tests/test_matrix.py -v
}

function run_dag_test()
{
  printf "Running dag test\n"
  dag/src/matrix.py --test |  python tests/test_dag.py -v
}

function run_example()
{
  root_uid=1
  node_uid=5
  printf "Example: Finding depth between nodes %d and %d\n" ${root_uid} ${node_uid}
  dag/src/matrix.py --test | python dag/src/dag.py --root ${root_uid} --node ${node_uid}
}

run_matrix_test
run_dag_test
run_example
