# README

## Challenge 1

Calculate the depth of a node within a DAG. The depth of a node is the lowest
number of edges between the node and root of the DAG. The DAG is encoded as an
adjacency matrix. The solution is implemented in Python (>=3.6) and the code is
documented using docstrings and
[`typing`](https://docs.python.org/3/library/typing.html). Because the DAG is
unweighted, the solution implements a Breadth-First-Search to find the shortest
path between two nodes.

The [implementation](dag/src/dag.py) contains a method to resolve all paths
between two nodes and can be used for testing.


Because a DAG is assumed, no test for self-loops or cyclic properties.

The adjacency matrix is parsed from STDIN. [`dag/src/matrix.py`](dag/src/matrix.py)
creates adjacency matrices from STDIN. `dag/src/matrix.py --test` prints a
predefined matrix.


### Demo

[`challenge1.sh`](challenge1.sh) runs the tests and an example.

### Usage
```$ python dag/src/dag.py -h```

```$ python dag/src/matrix.py -h```

Find shortest path between node 3 and node 5 in the test matrix.

```$ dag/src/matrix.py --test| python dag/src/dag.py --root 3 --node 5```

### DAG

The DAG used to test and develop the solution is shown below.

![DAG used for testing](graphs/dag.png)

The corresponding adjacency matrix. Connections are depicted from row to column.

```
   n0 n1 n2 n3 n4 n5 n6
n0 0  1  0  1  0  0  0
n1 0  0  1  0  1  0  0
n2 0  0  0  0  0  1  1
n3 0  0  1  0  1  0  1
n4 0  0  0  0  0  1  1
n5 0  0  0  0  0  0  1
n6 0  0  0  0  0  0  0
```

### Tests

Test for the construction of the adjacency matrix is implemented in
[tests/test_matrix.py](tests/test_matrix.py).

Test to find the expected shortest path between node 0 and node 6 are
implemented in [tests/test_dag.py](tests/test_dag.py).


### Create an adjacency matrix

Example creating an adjacency matrix and finding the shortets path between node
1 and the furthest node in the graph.

```
for i in "0 1" "1 3" "1 2" "3 4" "2 4"; do echo $i; done  | \
  dag/src/matrix.py  |\
  dag/src/dag.py --root 1
```
