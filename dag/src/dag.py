#!/usr/bin/env python3
"""
..
  Copyright 2020

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import sys
import math
import argparse
from typing import List, Set

class Dag:
  """
  Implements a Direct Acyclic Graph without weights from an adjacency matrix.
  It can find all paths, the shortest path, and the depth of node within the
  DAG and a root node.
  """

  class Node:
    """Implements a Node in a DAG. It stores its connections, distance to the
    root, and its predecessor to retrace paths."""

    def __init__(self, uid:int):
      self.uid = int(uid)
      self.children = []
      self.dist = math.inf
      self.prev:int = None

    def add_child(self, uid:int):
      """Adds child/connection to the node."""
      self.children.append(uid)

  def __init__(self):
    self.nodes = {}
    self.paths = []
    self.shortest_path = []

  def parse(self):
    """Parses an adjacency matrix from STDIn into DAG nodes."""
    cols = 0
    rows = 0
    self.nodes[rows] = Dag.Node(rows)
    for i in sys.stdin:
      for j in i:
        if j == '\n':
          cols = 0
          rows += 1
          self.nodes[rows] = Dag.Node(rows)
          continue
        if int(j) == 1:
          self.nodes[rows].add_child(cols)
        cols += 1
    self.nodes.pop(rows)

  def resolve(self, root_uid:int, dest_uid:int):
    """Resolves all path between the nodes root_uid and dest_uid."""
    self.parse()
    path = []
    seen = set()
    if root_uid not in self.nodes:
      sys.exit("Root with uid {} not in graph".format(root_uid))
    if dest_uid is not None and dest_uid not in self.nodes:
      sys.exit("Destination node with uid {} not in graph".format(root_uid))
    self.dfs(root_uid, dest_uid, seen, path)
    if not self.paths:
      sys.exit("No paths between nodes {} and {}".format(root_uid, dest_uid))
    print("All paths between nodes {} and {}:".format(root_uid, dest_uid))
    for i in self.paths:
      print("\t", [x.uid for x in i])

  def short_path(self)->List[int]:
    """Returns the shortest path."""
    return [x.uid for x in self.shortest_path]

  def depth(self, root_uid:int, dest_uid:int)->int:
    """Finds shortest path between nodes root_uid and dest_uid to
    find the depth of fest_uid. Queue could be exchanged with the queue or
    deqeue module."""
    if root_uid == dest_uid:
      sys.exit("Root and node are the same: {}. Depth: 0".format(root_uid, dest_uid))
    self.parse()
    if dest_uid is None:
      dest_uid = len(self.nodes) - 1
    if self.nodes.get(dest_uid) is None:
      sys.exit("Destination node {} does not exist. Abort".format(dest_uid))
    if self.nodes.get(root_uid) is None:
      sys.exit("Root node {} does not exist. Abort".format(root_uid))
    self.nodes[root_uid].dist = 0
    queue = [root_uid]
    self.bfs(queue, dest_uid)
    n = self.nodes[dest_uid]
    self.shortest_path = [n]
    depth = n.dist
    while n.uid != root_uid:
      if self.nodes.get(n.prev) is None:
        sys.exit("No path between nodes {} and {}".format(root_uid, dest_uid))
      self.shortest_path.insert(0, self.nodes[n.prev])
      n = self.nodes[n.prev]
    if not self.shortest_path:
      sys.exit("No path between nodes {} and {}".format(root_uid, dest_uid))
    print("Shortest path between {} and {} has depth {}: {}".format(root_uid,
          dest_uid, depth, [x.uid for x in self.shortest_path]), file=sys.stderr)
    return depth

  def bfs(self, queue:List, dest_uid:int):
    """Non-recursive Breadth-First-Search ti find shortest path. BFS can be
    applied because no weights are used in the DAG."""
    while queue:
      n = self.nodes[queue.pop(0)]
      for i in n.children:
        if self.nodes[i].dist == math.inf:
          self.nodes[i].dist = 1 + n.dist
          self.nodes[i].prev = n.uid
          queue.append(i)
          if dest_uid is not None and i == dest_uid:
            return
    return

  def dfs(self, node_uid:int, dest_uid:int, seen:Set[int], path:List):
    """Recursive Depth First search to find all paths between two nodes in a
    DAG. path has to be explicitly created as new list. Python would otherwise
    modify the variable referencing 'path'."""
    node = self.nodes[node_uid]
    seen.add(node.uid)
    path = path + [node]

    if dest_uid is not None and node.uid == dest_uid:
      path = path + [node]
      self.paths.append(path)

    if dest_uid is None and not self.nodes[node_uid].children:
      path = path + [node]
      self.paths.append(path)

    for i in node.children:
      if i not in seen:
        self.dfs(i, dest_uid, seen, path)

    # Go one node back
    path.pop()
    seen.remove(node.uid)
    if not path:
      return

def main():
  ap = argparse.ArgumentParser('Find paths and node depths in a DAG')
  ap.add_argument('--root', type=int, help='Root node number. Default 0',
                  metavar='number', default=0)
  ap.add_argument('--node', type=int, help='Destination node. If none given, \
                  use node 6.', metavar='number')
  args = ap.parse_args()
  d = Dag()
  #d.resolve(args.root, args.node)
  d.depth(args.root, args.node)
  return 0

if __name__ == '__main__':
  main()
