#!/usr/bin/env python3
"""
..
  Copyright 2020

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import sys
import argparse
from typing import List, Tuple

class AdjaciencyMatrix:
  """
  Implements an adjacency matrix. The nodes are connected by reading
  STDIN. Expected format: sourcenode[space]targetnode.
  """
  test_nodes = ([(0,1), (0,3), (1,2), (1,4), (2,5), (2,6), (3,2), (3,4), (3,6),
                 (4,5), (4,6), (5,6)])

  def __init__(self, size:int=7):
    """Sets up matrix without connections"""
    self.size:int = size
    self.mtx:List[List] = []
    for i in range(size):
      self.mtx.append([0 for x in range(size)])

  def connect(self, src:int, dest:int):
    """Connects two nodes."""
    self.mtx[src][dest] = 1

  def connect_nodes(self, connections:Tuple[int,int])->List[List[int]]:
    """Creates connections from a list."""
    for i in connections:
      self.connect(i[0], i[1])
    return self.mtx

  def matrix(self)->List[List[int]]:
    """Returns matrix."""
    return self.mtx

  def show(self):
    """Pretty prints matrix."""
    for i in self.mtx:
      print(' '.join([str(x) for x in i]))

  def create_test(self):
    """Creates test matrix."""
    self.connect_nodes(AdjaciencyMatrix.test_nodes)

  def print_mtx(self):
    """Prints matrix to STDOUT"""
    for i in self.mtx:
      print(''.join([str(x) for x in i]))

def main():
  ap = argparse.ArgumentParser('Create an adjacency matrix. Add connections as \
                                source[space]dest from STDIN.')
  ap.add_argument('--test', action='store_true', help='Create predefined test matrix')
  args = ap.parse_args()

  if args.test:
    amtx = AdjaciencyMatrix()
    amtx.create_test()
    amtx.print_mtx()
    return 0

  nodes = []
  size = 0
  for i in sys.stdin:
    src, dest = i.strip().split(' ')
    size = max(max(int(src), int(dest)), size)
    nodes.append((int(src), int(dest)))
  amtx = AdjaciencyMatrix(size+1)
  amtx.connect_nodes(nodes)
  amtx.print_mtx()
if __name__ == '__main__':

  main()
