#!/usr/bin/env python3
"""
..
  Copyright 2020

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import io
import os
import sys
import json
import hashlib
import unittest


import dag
import matrix

testdir = os.path.dirname(os.path.abspath(__file__))

class MatrixTest(unittest.TestCase):
  """Tests adjacency matrix."""
  test_nodes = [(0,1), (0,3), (1,2), (1,4), (2,5), (2,6), (3,2), (3,4),
                      (3,6), (4,5), (4,6), (5,6)]
  expected_mtx = "0101000001010000000110010101000001100000010000000"

  def test_adjaciency_matrix(self):
    """Tests against expected test matrix."""
    mtx = matrix.AdjaciencyMatrix()
    mtx.create_test()
    amtx = ""
    for i in mtx.connect_nodes(MatrixTest.test_nodes):
      amtx += ''.join(str(x) for x in i)
    self.assertEqual(amtx, MatrixTest.expected_mtx)

if __name__ == '__main__':
    unittest.main()

