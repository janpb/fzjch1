#!/usr/bin/env python3
"""
..
  Copyright 2020

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import sys
import hashlib
import unittest


import dag


def hashpath(path):
  """Calculate hashes from node uids. Length 8 allows uids between
  -128 and +128. Should be enough fo the test. """
  h = hashlib.sha1()
  for i in path:
    h.update((i).to_bytes(8, sys.byteorder))
  return h.hexdigest()

expected_shortpath = [0, 3, 6]
expected_depth = len(expected_shortpath) - 1

class DagTest(unittest.TestCase):
  """Test Dag methods"""

  def test_depth(self, root=0, node=6):
    """Tests input graph for shortest path and depth."""
    d = dag.Dag()
    depth = d.depth(root, node)
    self.assertEqual(hashpath(d.short_path()),
                     hashpath(expected_shortpath),
                     "Path {} is not expected shortest path:{}".format(d.short_path(),
                                                                       expected_shortpath))
    self.assertEqual(depth,
                     expected_depth,
                     "Wrong depth: {}. Expected {}".format(depth, expected_depth))

if __name__ == '__main__':
  unittest.main()
